package br.com.sicredi.challenge.feature.events.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class EventDTO(
    @SerializedName("people") val people: List<PeopleDTO>?,
    @SerializedName("date") val date: Long?,
    @SerializedName("id") val id: String?,
    @SerializedName("title") val title: String?,
    @SerializedName("description") val description: String?,
    @SerializedName("image") val image: String,
    @SerializedName("longitude") val longitude: Float?,
    @SerializedName("latitude") val latitude: Float?,
    @SerializedName("price") val price: Float?
) : Parcelable