package br.com.sicredi.challenge.feature.events.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.sicredi.challenge.data.Resource
import br.com.sicredi.challenge.feature.events.business.DoCheckInUseCase
import br.com.sicredi.challenge.feature.events.model.EventDTO
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class CheckInViewModel(
    private val doCheckInUseCase: DoCheckInUseCase
) :
    ViewModel() {

    private val _checkInResult = MutableLiveData<Resource<Any>>()

    val checkInResult: LiveData<Resource<Any>> = _checkInResult

    fun doCheckIn(eventDTO: EventDTO) {
        viewModelScope.launch {
            _checkInResult.value = Resource.loading()
            doCheckInUseCase.invoke(eventDTO)
                .catch { e ->
                    _checkInResult.value = Resource.error(e.message ?: "error")
                }
                .collect {
                    _checkInResult.value = it
                }
        }
    }
}