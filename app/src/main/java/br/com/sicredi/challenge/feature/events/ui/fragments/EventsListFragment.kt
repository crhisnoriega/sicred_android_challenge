package br.com.sicredi.challenge.feature.events.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.sicredi.challenge.base.BaseFragment
import br.com.sicredi.challenge.data.Status
import br.com.southsystem.challenge.databinding.FragmentEventsListBinding
import br.com.sicredi.challenge.feature.events.model.EventDTO
import br.com.sicredi.challenge.feature.events.ui.EventsListActivity
import br.com.sicredi.challenge.feature.events.ui.adapter.EventsListAdapter
import br.com.sicredi.challenge.feature.events.viewmodel.CheckInViewModel
import br.com.sicredi.challenge.feature.events.viewmodel.EventsListViewModel
import br.com.southsystem.challenge.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.koin.android.viewmodel.ext.android.viewModel

enum class EVENT_ACTION {
    DETAILS, CHECK_IN
}

class EventsListFragment : BaseFragment() {

    companion object {
        fun newInstance() =
            EventsListFragment()
    }

    private val viewModel by viewModel<EventsListViewModel>()
    private val checkInViewModel by viewModel<CheckInViewModel>()

    private lateinit var binding: FragmentEventsListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEventsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        configureObservers()
        configureEventsList()

        viewModel.fetchEvents()
    }

    override fun onResume() {
        super.onResume()
        binding.progress.visibility = View.VISIBLE
    }

    private fun configureObservers() {
        viewModel.listResult.observe(this, Observer { data ->
            when (data.status) {
                Status.LOADING -> binding.progress.visibility = View.VISIBLE
                Status.SUCCESS -> {
                    populateList(data.data ?: listOf())
                    binding.progress.visibility = View.GONE
                }
                Status.ERROR -> {
                    binding.progress.visibility = View.GONE
                    showError(data.message, binding.snackBar)
                }
            }
        })

        checkInViewModel.checkInResult.observe(this, Observer { data ->
            when (data.status) {
                Status.LOADING -> binding.progress.visibility = View.VISIBLE
                Status.SUCCESS -> {
                    context?.let {
                        MaterialAlertDialogBuilder(it)
                            .setMessage(context?.getString(R.string.successfull_checkin))
                            .setPositiveButton("Ok", null)
                            .show()
                    }
                    binding.progress.visibility = View.GONE
                }
                Status.ERROR -> {
                    binding.progress.visibility = View.GONE
                    showError(data.message, binding.snackBar)
                }
            }
        })
    }

    private fun onClick(eventDTO: EventDTO, eventAction: EVENT_ACTION) {
        when (eventAction) {
            EVENT_ACTION.DETAILS -> {
                var activity = requireActivity() as? EventsListActivity
                activity?.let {
                    eventDTO.id?.let { id -> it.showEventDetails(id) }
                }
            }

            EVENT_ACTION.CHECK_IN -> {
                checkInViewModel.doCheckIn(eventDTO)
            }
        }
    }

    private fun configureEventsList() {
        binding.rvEvents.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter =
                EventsListAdapter(
                    mutableListOf(),
                    this@EventsListFragment::onClick
                )
        }
    }


    private fun populateList(eventsList: List<EventDTO>) {
        eventsList?.let {
            (binding.rvEvents.adapter as EventsListAdapter).apply {
                this.eventsList.addAll(eventsList.toMutableList())
                this.notifyDataSetChanged()
            }
        }
    }

}