package br.com.sicredi.challenge.feature.events.business

import br.com.sicredi.challenge.base.BaseUseCase
import br.com.sicredi.challenge.data.Resource
import br.com.sicredi.challenge.feature.events.model.EventDTO
import br.com.sicredi.challenge.feature.events.repository.EventsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DoCheckInUseCase(
    private val eventsRepository: EventsRepository
) : BaseUseCase<EventDTO, Any>(
    Dispatchers.IO
) {
    override fun execute(eventDTO: EventDTO): Flow<Resource<Any>> {
        return eventsRepository.doCheckIn(eventDTO).map {
            Resource.success("")
        }
    }
}