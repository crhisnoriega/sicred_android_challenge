package br.com.sicredi.challenge.feature.events.persistence

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow


@Dao
interface EventDAO {

    @Query("SELECT * FROM EventEntity")
    fun getAll(): Flow<List<EventEntity>>

}
