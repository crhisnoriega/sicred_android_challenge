package br.com.sicredi.challenge.feature.events.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.sicredi.challenge.data.Resource
import br.com.sicredi.challenge.feature.events.business.FetchEventsUseCase
import br.com.sicredi.challenge.feature.events.business.RetrieveEventUseCase
import br.com.sicredi.challenge.feature.events.model.EventDTO
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class EventsListViewModel(
    private val fetchEventsUseCase: FetchEventsUseCase,
    private val retrieveEventUseCase: RetrieveEventUseCase
) :
    ViewModel() {

    private val _listResult = MutableLiveData<Resource<List<EventDTO>>>()
    private val _eventResult = MutableLiveData<Resource<EventDTO>>()

    val listResult: LiveData<Resource<List<EventDTO>>> = _listResult
    val eventResult: LiveData<Resource<EventDTO>> = _eventResult

    fun fetchEvents() {
        viewModelScope.launch {
            _listResult.value = Resource.loading()
            fetchEventsUseCase.invoke(null)
                .catch { e ->
                    _listResult.value = Resource.error(e.message ?: "error")
                }
                .collect { value ->
                    _listResult.value = value
                }
        }
    }

    fun fetchEvent(eventID: String) {
        viewModelScope.launch {
            _listResult.value = Resource.loading()
            retrieveEventUseCase.invoke(eventID)
                .catch { e ->
                    _eventResult.value = Resource.error(e.message ?: "error")
                }
                .collect { value ->
                    _eventResult.value = value
                }
        }
    }
}