package br.com.sicredi.challenge.feature.events.ui

import android.os.Bundle
import br.com.southsystem.challenge.R
import br.com.sicredi.challenge.base.BaseActivity
import br.com.sicredi.challenge.feature.events.di.EventsModule
import br.com.sicredi.challenge.feature.events.ui.fragments.EventDetailsFragment
import br.com.sicredi.challenge.feature.events.ui.fragments.EventsListFragment
import org.koin.core.module.Module

class EventsListActivity : BaseActivity() {


    override val modules: List<Module> = listOf(EventsModule.modules)
    override val contentView = R.layout.main_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        showEventsList()
    }

    override fun onBackPressed() {
        showEventsList()
    }

    private fun showEventsList() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, EventsListFragment.newInstance())
            .commit()
    }

    fun showEventDetails(eventID: String) {
        val fragments = supportFragmentManager.fragments
        fragments.forEach {
            supportFragmentManager.beginTransaction().remove(it).commit()
        }
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, EventDetailsFragment.newInstance(eventID), eventID)
            .commit()
    }
}