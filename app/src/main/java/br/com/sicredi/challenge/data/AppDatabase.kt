package br.com.sicredi.challenge.data

import androidx.room.Database
import androidx.room.RoomDatabase
import br.com.sicredi.challenge.feature.events.persistence.EventDAO
import br.com.sicredi.challenge.feature.events.persistence.EventEntity

const val DATABASE_NAME = "events-db"
@Database
    (entities = [EventEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun eventDAO(): EventDAO
}