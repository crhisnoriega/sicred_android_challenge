package br.com.sicredi.challenge.feature.events.api

import br.com.sicredi.challenge.feature.events.model.EventDTO
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface EventsAPI {

    @GET("/api/events")
    suspend fun fetchEvents(
    ): List<EventDTO>

    @GET("/api/events/{event_id}")
    suspend fun retrieveEvent(
        @Path("event_id") eventID: String
    ): EventDTO

    @POST("/api/checkin")
    suspend fun doCheckIn(
        @Body eventDTO: EventDTO
    ): Response<Any>

}