package br.com.sicredi.challenge.feature.events.business

import br.com.sicredi.challenge.base.BaseUseCase
import br.com.sicredi.challenge.data.Resource
import br.com.sicredi.challenge.feature.events.model.EventDTO
import br.com.sicredi.challenge.feature.events.persistence.EventDAO
import br.com.sicredi.challenge.feature.events.repository.EventsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class FetchEventsUseCase(
    private val eventsRepository: EventsRepository,
    private val eventDAO: EventDAO
) : BaseUseCase<Void?, List<EventDTO>>(
    Dispatchers.IO
) {
    override fun execute(parameters: Void?): Flow<Resource<List<EventDTO>>> {
        // qualquer outro tratamento no response pode ser adicionado aqui
        // p.e.: join com cache em banco local
        return eventsRepository.fetchEvents().map {
            Resource.success(it)
        }
    }
}