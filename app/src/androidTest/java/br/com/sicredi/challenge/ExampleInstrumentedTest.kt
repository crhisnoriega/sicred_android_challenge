package br.com.sicredi.challenge

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import br.com.sicredi.challenge.feature.events.ui.EventsListActivity
import br.com.southsystem.challenge.R
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @get:Rule
    var mActivityRule = ActivityTestRule(EventsListActivity::class.java, false, false)

    @Before
    fun init() {
        mActivityRule.launchActivity(null)
    }

    @Test
    fun test101ClickConvert() {
        onView(withId(R.id.rv_events)).check(matches(isDisplayed()))
    }
}